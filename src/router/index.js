import Vue from 'vue'
import Router from 'vue-router'
import HomeMovies from '@/components/Movies/HomeMovies'
import TvSingleComponent from '@/components/Series/TvSingleComponent'
import MovieSingle from '@/components/Movies/MovieSingle'
import HomeCinema from '@/components/Cinema/HomeCinema'
import HomeTV from '@/components/Series/HomeTV'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HomeMovies',
      component: HomeMovies
    },
    {
      path: '/movie',
      name: 'HomeMovies',
      component: HomeMovies
    },
    {
      path: '/movie/:id',
      name: 'MovieSingle',
      component: MovieSingle
    },
    {
      path: '/tv',
      name: 'HomeTV',
      component: HomeTV
    },
    {
      path: '/tv/:id',
      name: 'TvSingleComponent',
      component: TvSingleComponent
    },
    {
      path: '/cinema',
      name: 'HomeCinema',
      component: HomeCinema
    },
  ]
})
