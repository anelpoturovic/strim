import firebase from 'firebase'
import firestore from 'firebase/firestore'

var config = {
    apiKey: "AIzaSyBqubkUiaiJYu9TMNrRVTOpx4gbQTNRo84",
    authDomain: "strim-f95f8.firebaseapp.com",
    databaseURL: "https://strim-f95f8.firebaseio.com",
    projectId: "strim-f95f8",
    storageBucket: "strim-f95f8.appspot.com",
    messagingSenderId: "484766110127",
    appId: "1:484766110127:web:13091b2c37bccb55"
};
const firebaseApp = firebase.initializeApp(config);

export default firebaseApp.firestore( )